package com.example.jenkinssample;

public class Calculator {
    public static int total(int price, int number) {
        if (price < 0 || number < 0) {
            throw new IllegalArgumentException("価格と個数は0以上で設定してください");
        }
        return price * number;
    }
}
