package com.example.jenkinssample;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class CalculatorTest {

    @Test
    public void 価格100個数3でtotalメソッドを呼び出すと300が返る() {
        assertEquals(300, Calculator.total(100, 3));
    }

    @Test
    public void 価格マイナス1個数1でtotalメソッドを呼び出すとIllegalArgumentExceptionがThrowされる() {
        assertThrows(IllegalArgumentException.class, () -> Calculator.total(-1, 1));
    }
}
